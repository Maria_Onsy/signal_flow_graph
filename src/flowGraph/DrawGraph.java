package flowGraph;

import java.awt.Graphics;
import java.awt.Point;
import java.util.List;



public interface DrawGraph {
	/*
	 * draw the graph
	 */
public void refresh(Graphics g);
/*
 * draw the given node
 */
public void drawNodes(Graphics g,INode node,int i);
/*
 * draw the given edge
 */
public void drawEdge(Graphics g,IEdge edge);
/*
 * delete the drawn graph
  */
public void clear();
}
