package flowGraph;

public interface IEdge {
	
/*
* set the value of the edge
*/
public void setValue(double value);
/*
 * return the value of the edge
 */
public double getValue();
/*
 * set the sign of the edge
 */
public void setPositive(boolean p);
/*
 * is the value of the edge positive
 */
public boolean isPositive();
/*
 * set the node which the edge is moving to
 */
public void setToNode(INode nodes);
/*
 * return the node which the edge is moving to
 */
public INode getToNode();
/*
 * set the node which the edge is from
 */
public void setFromNode(INode nodes);
/*
 * return the node which the edge is from
 */
public INode getFromNode();
}
