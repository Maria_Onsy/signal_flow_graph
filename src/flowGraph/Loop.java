package flowGraph;

import java.util.ArrayList;
import java.util.List;

public class Loop implements ILoop{

	private double value;
	private INode theNode;
	private List<INode> loopNodes=new ArrayList<INode>();
	private List<IEdge> loopEdges=new ArrayList<IEdge>();
	
	
	 public Loop(double value, INode theNode, List<INode> loopNodes,List<IEdge> loopEdges) {
		this.value = value;
		this.theNode = theNode;
		this.loopNodes = loopNodes;
		this.loopEdges = loopEdges;
	}

	@Override
	public double getLoopValue() {
		return value;
	}
	 
	 @Override
	public void setLoopValue(double value) {
		this.value = value;
	}
	 
	 @Override
	public INode getTheNode() {
		return theNode;
	}
	 
	 @Override
	public void setTheNode(INode theNode) {
		this.theNode = theNode;
	}
	 
	 @Override
	public List<INode> getLoopNodes() {
		return loopNodes;
	}
	 
	 @Override
	public void setLoopNodes(List<INode> loopNodes) {
		this.loopNodes = loopNodes;
	}
	 
	 @Override
	public List<IEdge> getLoopEdges() {
		return loopEdges;
	}
	 
	 @Override
	public void setLoopEdges(List<IEdge> loopEdges) {
		this.loopEdges = loopEdges;
	}

    
}
