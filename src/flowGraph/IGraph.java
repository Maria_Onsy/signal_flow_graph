package flowGraph;

import java.util.List;

public interface IGraph {

	/*
	 * insert new node to the graph
	 */
	public void insertNode(INode n);
	/*
	 * insert new edge to the graph
	 */
	public void insertEdge(IEdge e);
	/*
	 * return the node using its name
	 */
	public INode getNode(String name);
	/*
	 * return all nodes in the graph
	 */
	public List<INode> getNodes();
	/*
	 * return all edges in the graph
	 */
	public List<IEdge> getEdges();
	/*
	 * return the positive edges in the graph
	 */
	public List<IEdge> getPosEdges();
	/*
	 * return the negative edges in the graph
	 */
	public List<IEdge> getNegEdges();
	/*
	 * return all forward paths between the given two nodes
	 */
	public List<IPath> getPaths(INode node,INode last);
	/*
	 * return all loops in the graph
	 */
	public List<ILoop> getLoops();
	/*
	 * return all untouched loops in the given loops
	 */
	public List<List<List<ILoop>>> getUnTouLoops(List<ILoop> lp);
	/*
	 * return the index of the given edge
	 */
	int getEdge(IEdge e);
	/*
	 * clear the graph
	 */
	public void clear();
}
