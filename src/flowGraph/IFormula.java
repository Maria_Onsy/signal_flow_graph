package flowGraph;

public interface IFormula {
	
/*
 * calculate delta	
 */
public double calDelta();
/*
 * calculate delta for the given path
 */
public double calDeltaK(IPath p);
/*
 * calculate the gain
 */
public double getGain();
}
