package flowGraph;

import java.util.ArrayList;
import java.util.List;


public class Node implements INode{
    private String name;
    private List<INode> nextNodes=new ArrayList<INode>();
    private List<INode> previousNodes=new ArrayList<INode>();
    private List<IEdge> forwardEdges=new ArrayList<IEdge>();
    private List<IEdge> backwardEdges=new ArrayList<IEdge>();
    public Node() {}
    public Node(String name,List<INode> nextNodes,List<INode> previousNodes,List<IEdge> forwardEdges,List<IEdge> backwardEdges) {
		this.name=name;
		this.nextNodes=nextNodes;
		this.previousNodes=previousNodes;
		this.forwardEdges=forwardEdges;
		this.backwardEdges=backwardEdges;
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public List<INode> getNextNodes() {
		return nextNodes;
	}

	@Override
	public List<INode> getPreviousNodes() {
		return previousNodes;
	}

	@Override
	public List<IEdge> getForwardEdges() {
		return forwardEdges;
	}

	@Override
	public List<IEdge> getBackwardEdges() {
		return backwardEdges;
	}

	@Override
	public void setName(String name) {
		this.name=name;
	}

	@Override
	public void setNextNodes(List<INode> nodes) {
		 this.nextNodes=nodes;
	}

	@Override
	public void setPreviousNodes(List<INode> nodes) {
	     this.previousNodes=nodes;
	}

	@Override
	public void setForwardEdges(List<IEdge> edges) {
		this.forwardEdges=edges;
	}

	@Override
	public void setBackwardEdges(List<IEdge> edges) {
		this.backwardEdges=edges;
	}

}
