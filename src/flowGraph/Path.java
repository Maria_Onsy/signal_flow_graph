package flowGraph;

import java.util.ArrayList;
import java.util.List;

public class Path implements IPath {

    private List<INode> pathNodes=new ArrayList<INode>();
    private List<IEdge> pathEdges=new ArrayList<IEdge>();
    private double pathValue;
    
    public Path(List<INode> pathNodes, List<IEdge> pathEdges, double pathValue) {
		this.pathNodes = pathNodes;
		this.pathEdges = pathEdges;
		this.pathValue = pathValue;
	}

	@Override
	public List<INode> getPathNodes() {
		return pathNodes;
	}
    
    @Override
	public void setPathNodes(List<INode> pathNodes) {
		this.pathNodes = pathNodes;
	}
    
    @Override
	public List<IEdge> getPathEdges() {
		return pathEdges;
	}
    
    @Override
	public void setPathEdges(List<IEdge> pathEdges) {
		this.pathEdges = pathEdges;
	}
    
    @Override
	public double getPathValue() {
		return pathValue;
	}
    
    @Override
	public void setPathValue(double pathValue) {
		this.pathValue = pathValue;
	}
    
	
}
