package flowGraph;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

public class Graph  implements IGraph{
	private List<INode> nodes=new ArrayList<INode>();
	private List<IEdge> edges=new ArrayList<IEdge>();
    private List<IPath> paths=new ArrayList<IPath>();
    private List<ILoop> loops=new ArrayList<ILoop>();
    boolean fp=false;
    boolean fl=false;
    


	@Override
	public void insertNode(INode n) {
		for(int i=0;i<nodes.size();i++) {
		if(nodes.get(i).getName().equals(n.getName())) {
			JOptionPane.showMessageDialog(null, "This node is already exist", "Error", JOptionPane.ERROR_MESSAGE);
			return;
		}}
		nodes.add(n);
	}

	@Override
	public void insertEdge(IEdge e) {
		if(getEdge(e)!=-1) {
			JOptionPane.showMessageDialog(null, "This edge is already exist", "Error", JOptionPane.ERROR_MESSAGE);
			return;

		}
	   edges.add(e);
	   INode f=getNode(e.getFromNode().getName());
	   INode b=getNode(e.getToNode().getName());
	   if(e.isPositive()) {
		   f.getNextNodes().add(b);
		   f.getForwardEdges().add(e);
	   }
	   else {
		   b.getPreviousNodes().add(f);
		   b.getBackwardEdges().add(e);
	   }
	}


	@Override
	public INode getNode(String name) {
		for(int i=0;i<nodes.size();i++) {
			if(nodes.get(i).getName().equals(name)) {
				return nodes.get(i);
			}
		}
		return null;
	}

	@Override
	public int getEdge(IEdge e) {
		for(int i=0;i<edges.size();i++) {
			if(edges.get(i).getValue()==e.getValue()){
				if(edges.get(i).getFromNode().equals(e.getFromNode())) {
					if(edges.get(i).getToNode().equals(e.getToNode())) {
						return i;
					}
				}
			}
		}
		return -1;
	}

	@Override
	public List<INode> getNodes() {
		return nodes;
	}

	@Override
	public List<IEdge> getEdges() {
		return edges;
	}

	@Override
	public List<IEdge> getPosEdges() {
		List<IEdge> posEdges = new ArrayList<IEdge>();
		for(int i=0;i<edges.size();i++) {
			if(edges.get(i).isPositive()) {
				posEdges.add(edges.get(i));
			}
		}
		return posEdges;
	}

	@Override
	public List<IEdge> getNegEdges() {
		List<IEdge> negEdges = new ArrayList<IEdge>();
		for(int i=0;i<edges.size();i++) {
			if(!(edges.get(i).isPositive())) {
				negEdges.add(edges.get(i));
			}
		}
		return negEdges;
	}

	@Override
	public List<IPath> getPaths(INode node,INode last) {
		List<IPath> paths=new ArrayList<IPath>();
		if(node==last) {
			List<INode> pathNodes=new ArrayList<INode>();
			List<IEdge> pathEdges=new ArrayList<IEdge>(); 
			double pathValue=1;
			pathNodes.add(node);
			IPath p=new Path(pathNodes, pathEdges, pathValue);
			paths.add(p);
			return paths;
		}
		List<INode> secNodes=node.getNextNodes();
		List<IEdge> secEdges=node.getForwardEdges();
		for(int i=0;i<secNodes.size();i++) {
			List<IPath> secPaths=new ArrayList<IPath>();
			if(secNodes.get(i)!=last) {
				secPaths=getPaths(secNodes.get(i),last);
			for(int j=0;j<secPaths.size();j++) {
				secPaths.get(j).getPathNodes().add(0, node);
				secPaths.get(j).getPathEdges().add(0, secEdges.get(i));
				secPaths.get(j).setPathValue(secPaths.get(j).getPathValue()*secEdges.get(i).getValue());
			 }
			}
			else {
				List<INode> pathNodes=new ArrayList<INode>();
				List<IEdge> pathEdges=new ArrayList<IEdge>(); 
				double pathValue=1;
				pathNodes.add(node);
				pathNodes.add(secNodes.get(i));
				pathEdges.add(secEdges.get(i));
				pathValue=pathValue*secEdges.get(i).getValue();
				IPath p=new Path(pathNodes, pathEdges, pathValue);
				secPaths.add(p);
			}
			paths.addAll(secPaths);
		}
		return paths;
	}
	
	
	@Override
	public List<ILoop> getLoops() {
		if(fl==false) {
		List<ILoop> loops=new ArrayList<ILoop>();
		List <IEdge> negEdges=getNegEdges();
		for(int i=0;i<negEdges.size();i++) {
			List<IPath> p=getPaths(negEdges.get(i).getToNode(),negEdges.get(i).getFromNode());
			for(int j=0;j<p.size();j++) {
				p.get(j).getPathNodes().add(negEdges.get(i).getToNode());
				p.get(j).getPathEdges().add(negEdges.get(i));
				p.get(j).setPathValue(p.get(j).getPathValue()*negEdges.get(i).getValue());
				ILoop l=new Loop(p.get(j).getPathValue(),negEdges.get(i).getToNode(),p.get(j).getPathNodes(),p.get(j).getPathEdges());
				loops.add(l);
			}
		}
		this.loops=loops;
		fl=true;
		}
		return this.loops;
		
	}

	@Override
	public List<List<List<ILoop>>> getUnTouLoops(List<ILoop> lp) {
		List<List<List<ILoop>>> unTou=new ArrayList<List<List<ILoop>>>();
		List<List<ILoop>> secl=new ArrayList<List<ILoop>>();
		unTou.add(secl);
		for(int i=0;i<lp.size();i++) {
			ILoop l=lp.get(i);
			for(int j=i+1;j<lp.size();j++) {
				boolean f=true;
				for(int k=0;k<l.getLoopNodes().size();k++) {
					for(int m=0;m<lp.get(j).getLoopNodes().size();m++) {
						if(l.getLoopNodes().get(k).getName().equals(lp.get(j).getLoopNodes().get(m).getName())) {
							f=false;
							break;
						}
					}
					if(!f) {break;}
				}
				if(f) {
					List<ILoop> tLoops=new ArrayList<ILoop>();
					tLoops.add(l);
					tLoops.add(lp.get(j));
					secl.add(tLoops);
					int y=j+1;
					if(y<lp.size()) {
						List<ILoop> union=new ArrayList<ILoop>();
						union.add(l);
						union.add(lp.get(j));
						getMoreUnTouch(union, y, unTou);
					}
				}
			}
		}
		return unTou;
	}
  
	public void getMoreUnTouch(List<ILoop> union,int y,List<List<List<ILoop>>> unTou) {
		List<List<ILoop>> mul=getUnTouLoops(union, y);
		if(mul.size()==0) {return;}
		if(unTou.size()<mul.get(0).size()-1) {
			unTou.add(mul);
		}
		else {unTou.get(mul.get(0).size()-2).addAll(mul);}
		for(int i=0;i<mul.size();i++) {
            int w=loops.indexOf(mul.get(i).get(mul.get(i).size()))+1;
            if(w<loops.size()) {
			getMoreUnTouch(mul.get(i),w+1, unTou);}
		}
	}
	public List<List<ILoop>> getUnTouLoops(List<ILoop> l,int j) {
		List<INode> nodes=new ArrayList<INode>();
		for(int i=0;i<l.size();i++) {
			nodes.addAll(l.get(i).getLoopNodes());
		}
		List<List<ILoop>> secl=new ArrayList<List<ILoop>>();
		for(int i=j;i<loops.size();i++) {
				boolean f=true;
				for(int k=0;k<nodes.size();k++) {
					for(int m=0;m<loops.get(i).getLoopNodes().size();m++) {
						if(nodes.get(k).getName().equals(loops.get(i).getLoopNodes().get(m).getName())) {
							f=false;
							break;
						}
					}
					if(!f) {break;}
				}
				if(f) {
					List<ILoop> tLoops=new ArrayList<ILoop>();
					tLoops.addAll(l);
					tLoops.add(loops.get(i));
					secl.add(tLoops);
				}
			}
		return secl;
	}


	@Override
	public void clear() {
		nodes.clear();
		edges.clear();
		paths.clear();
		loops.clear();
	}

}
