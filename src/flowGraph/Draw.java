package flowGraph;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Path2D;
import java.util.ArrayList;
import java.util.List;


public class Draw implements DrawGraph{
	List<Double> x=new ArrayList<Double>();
	List<Shape> n=new ArrayList<Shape>();
	IGraph graph;
	int l=2;
	List<Integer> fline=new ArrayList<Integer>();
	List<Integer> bline=new ArrayList<Integer>();
	int e=0;
	int i=0;
	int b=2;
	
	
	Draw(IGraph graph){
		this.graph=graph;
	}
    
	@Override
	public void refresh(Graphics g) {
		for(int i=0;i<graph.getNodes().size();i++) {
			drawNodes(g, graph.getNodes().get(i),i);
		}
		i=0;
		e=0;
		for(int i=0;i<graph.getEdges().size();i++) {
			drawEdge(g, graph.getEdges().get(i));
		}
		
	}

	@Override
	public void drawNodes(Graphics g,INode node,int i) {
		double del=860/(graph.getNodes().size()+1);
		Shape s=new Ellipse2D.Double(del*(i+1), 255, 40, 28);
		g.drawOval((int)del*(i+1), 255,40 ,28);
		g.drawString(node.getName(),(int)del*(i+1)+15 ,255+15 );
		if(!n.contains(s)) {
			x.add(del*(i+1));
			n.add(s);}
		else {x.set(i,del*(i+1));}
	}
	
	@Override
	public void drawEdge(Graphics g,IEdge edge) {
		int x1=(int)((double)x.get(graph.getNodes().indexOf(edge.getFromNode())))+20;
		int x2=(int)((double)x.get(graph.getNodes().indexOf(edge.getToNode())))+20;
		int y1=265;
		int y2=265;
		if(edge.isPositive()) {
			int p1=edge.getFromNode().getForwardEdges().size();
			boolean pos=(edge.getFromNode().getNextNodes().get(0)==edge.getToNode());
			if(p1==1||pos) {
				g.drawLine((x1+x2)/2 ,y1-5, (x1+x2)/2, y1+5);
				g.drawLine((x1+x2)/2 ,y1+5, ((x1+x2)/2)+10, y1);
				g.drawLine((x1+x2)/2 ,y1-5, ((x1+x2)/2)+10, y1);
				g.drawLine(x1+15, y1, x2-15, y2);
				g.drawString(Double.toString(edge.getValue()), (int)((x1+x2)/2), (int)(y1+20));	
				return;
			}
			else {
				int k=0;
				if(graph.getEdge(edge)==graph.getEdges().size()-1) {
					k=l;
					l++;
					fline.add(k);}
				else {k=fline.get(i);i++;}
				g.drawLine((x1+x2)/2 ,(y1-30*k)-5, (x1+x2)/2, (y1-30*k)+5);
				g.drawLine((x1+x2)/2 ,(y1-30*k)-5, ((x1+x2)/2)+10, y1-30*k);
				g.drawLine((x1+x2)/2 ,(y1-30*k)+5, ((x1+x2)/2)+10, y1-30*k);
				g.drawLine(x1,y1-10 , x1, y1-30*k);
				g.drawLine(x2,y2-10 , x2, y2-30*k);
				g.drawLine(x1,y1-30*k , x2, y2-30*k);
				g.drawString(Double.toString(edge.getValue()), (int)((x1+x2)/2), (int)((y1-30*k)+20));	
				return;
			}
			}
		else {
			if(edge.getToNode()==edge.getFromNode()) {
				g.drawLine((x1+x2)/2 ,(y1+40)-5, (x1+x2)/2, (y1+40)+5);
				g.drawLine((x1+x2)/2 ,(y1+40)-5, ((x1+x2)/2)-10, y1+40);
				g.drawLine((x1+x2)/2 ,(y1+40)+5, ((x1+x2)/2)-10, y1+40);
				Graphics2D g2=(Graphics2D) g;
				Path2D.Double pa=new Path2D.Double(); 
				pa.moveTo(x1-15, y1);
				pa.curveTo(x1-15, y1, x1, y1+80, x1+25, y1);
				g2.draw(pa);
				g.drawString(Double.toString(edge.getValue()), (int)((x1+x2)/2), (int)(y1+50));
			}
			else {
				int k=0;
				if(graph.getEdge(edge)==graph.getEdges().size()-1) {
					k=b;
					b++;
					bline.add(k);}
				else {k=bline.get(e);e++;}
					g.drawLine((x1+x2)/2 ,(y1+30*k)-5, (x1+x2)/2, (y1+30*k)+5);
					g.drawLine((x1+x2)/2 ,(y1+30*k)-5, ((x1+x2)/2)-10, y1+30*k);
					g.drawLine((x1+x2)/2 ,(y1+30*k)+5, ((x1+x2)/2)-10, y1+30*k);
					g.drawLine(x1,y1+10 , x1, y1+30*k);
					g.drawLine(x2,y2+10 , x2, y2+30*k);
					g.drawLine(x1,y1+30*k , x2, y2+30*k);
					g.drawString(Double.toString(edge.getValue()), (int)((x1+x2)/2), (int)((y1+30*k)+20));	
					return;
			    }
				}
		
	}

	@Override
	public void clear() {
		n.clear();
		l=2;
		fline.clear();
		bline.clear();
		e=0;
		i=0;
		b=2;
	}
}