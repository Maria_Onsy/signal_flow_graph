package flowGraph;

import java.util.ArrayList;
import java.util.List;

public class test {
Graph graph=new Graph();
public void test1() {
INode n1=new Node();
INode n2=new Node();
INode n3=new Node();
INode n4=new Node();
INode n5=new Node();
INode n6=new Node();
//edges
IEdge e1=new Edge(1,true, n2,n1);
IEdge e2=new Edge(5,true, n3,n2);
IEdge e3=new Edge(10,true, n4,n3);
IEdge e4=new Edge(2,true, n5,n4);
IEdge e5=new Edge(10,true, n6,n2);
IEdge e6=new Edge(2,true, n5,n6);
IEdge e7=new Edge(-1,false, n6,n6);
IEdge e8=new Edge(-1,false, n3,n4);
IEdge e9=new Edge(-2,false, n4,n5);
IEdge e10=new Edge(-1,false, n2,n5);
//nextNodes
List<INode> nn1=new ArrayList<INode>();
List<INode> nn2=new ArrayList<INode>();
List<INode> nn3=new ArrayList<INode>();
List<INode> nn4=new ArrayList<INode>();
List<INode> nn6=new ArrayList<INode>();
//PreviousNodes
List<INode> np2=new ArrayList<INode>();
List<INode> np3=new ArrayList<INode>();
List<INode> np4=new ArrayList<INode>();
List<INode> np6=new ArrayList<INode>();
//forwardEdges
List<IEdge> nf1=new ArrayList<IEdge>();
List<IEdge> nf2=new ArrayList<IEdge>();
List<IEdge> nf3=new ArrayList<IEdge>();
List<IEdge> nf4=new ArrayList<IEdge>();
List<IEdge> nf6=new ArrayList<IEdge>();
//backwardEdges
List<IEdge> nb2=new ArrayList<IEdge>();
List<IEdge> nb3=new ArrayList<IEdge>();
List<IEdge> nb4=new ArrayList<IEdge>();
List<IEdge> nb6=new ArrayList<IEdge>();
//add to nextNodes
nn1.add(n2);
nn2.add(n3);nn2.add(n6);
nn3.add(n4);
nn4.add(n5);
nn6.add(n5);
//add to previousNodes
np2.add(n5);
np3.add(n4);
np4.add(n5);
np6.add(n6);
//add to forwardEdges
nf1.add(e1);
nf2.add(e2);nf2.add(e5);
nf3.add(e3);
nf4.add(e4);
nf6.add(e6);
//add to backwardEdges
nb2.add(e10);
nb3.add(e8);
nb4.add(e9);
nb6.add(e7);
//add to nodes
n1.setName("y1");
n1.setNextNodes(nn1);
n1.setPreviousNodes(null);
n1.setForwardEdges(nf1);
n1.setBackwardEdges(null);

n2.setName("y2");
n2.setNextNodes(nn2);
n2.setPreviousNodes(np2);
n2.setForwardEdges(nf2);
n2.setBackwardEdges(nb2);

n3.setName("y3");
n3.setNextNodes(nn3);
n3.setPreviousNodes(np3);
n3.setForwardEdges(nf3);
n3.setBackwardEdges(nb3);

n4.setName("y4");
n4.setNextNodes(nn4);
n4.setPreviousNodes(np4);
n4.setForwardEdges(nf4);
n4.setBackwardEdges(nb4);

n5.setName("y5");
n5.setNextNodes(null);
n5.setPreviousNodes(null);
n5.setForwardEdges(null);
n5.setBackwardEdges(null);

n6.setName("y6");
n6.setNextNodes(nn6);
n6.setPreviousNodes(np6);
n6.setForwardEdges(nf6);
n6.setBackwardEdges(nb6);

//add to graph
graph.insertNode(n1);
graph.insertNode(n2);
graph.insertNode(n3);
graph.insertNode(n4);
graph.insertNode(n5);
graph.insertNode(n6);
graph.insertEdge(e1);
graph.insertEdge(e2);
graph.insertEdge(e3);
graph.insertEdge(e4);
graph.insertEdge(e5);
graph.insertEdge(e6);
graph.insertEdge(e7);
graph.insertEdge(e8);
graph.insertEdge(e9);
graph.insertEdge(e10);

//check paths
List<IPath> pt=graph.getPaths(n1,n5);
List<ILoop> lt=graph.getLoops();
List<List<List<ILoop>>> untList=graph.getUnTouLoops(lt);
IFormula m=new MasonFormula(graph, n1, n5);
double d=m.calDelta();
double g=m.getGain();
int y = 0;
y++;
}

}
