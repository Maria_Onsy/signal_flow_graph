package flowGraph;

public class Edge  implements IEdge{
private double value;
private boolean positive;
private INode toNode;
private INode fromNode;

public Edge(double value, boolean positive, INode toNode, INode fromNode) {
	this.value = value;
	this.positive = positive;
	this.toNode = toNode;
	this.fromNode = fromNode;
}

@Override
public double getValue() {
	return value;
}

@Override
public void setValue(double value) {
	this.value = value;
}

@Override
public boolean isPositive() {
	return positive;
}

@Override
public void setPositive(boolean positive) {
	this.positive = positive;
}

@Override
public INode getToNode() {
	return toNode;
}

@Override
public void setToNode(INode toNode) {
	this.toNode = toNode;
}

@Override
public INode getFromNode() {
	return fromNode;
}

@Override
public void setFromNode(INode fromNode) {
	this.fromNode = fromNode;
}
}
