package flowGraph;

import java.util.ArrayList;
import java.util.List;

public class MasonFormula implements IFormula {
    
	IGraph graph=new Graph();
	INode firstNode;
	INode lastNode;
	List<IPath> paths;
	List<ILoop> loops;
	List<List<List<ILoop>>> unTouLoop;
	
	public MasonFormula(IGraph graph, INode firstNode, INode lastNode) {
		super();
		this.graph = graph;
		this.firstNode = firstNode;
		this.lastNode = lastNode;
		this.paths=graph.getPaths(firstNode,lastNode);
		this.loops=graph.getLoops();
		this.unTouLoop=graph.getUnTouLoops(loops);
	}

	@Override
	public double calDelta() {
		double delta=1;
		for(int i=0;i<loops.size();i++) {
			delta=delta-loops.get(i).getLoopValue();
		}
		for(int i=0;i<unTouLoop.size();i++) {
			List<List<ILoop>> s=unTouLoop.get(i);
			for(int j=0;j<s.size();j++) {
				double v=1;
				for(int k=0;k<s.get(j).size();k++) {
					v=v*s.get(j).get(k).getLoopValue();
				}
				delta=delta+Math.pow(1,i)*v;
			}
		}
		return delta;
	}

	@Override
	public double calDeltaK(IPath p) {
		List<ILoop> loopsk=removeLoops(p);
		List<List<List<ILoop>>> unTouLoopk=graph.getUnTouLoops(loopsk);
		double delta=1;
		for(int i=0;i<loopsk.size();i++) {
			delta=delta-loopsk.get(i).getLoopValue();
		}
		for(int i=0;i<unTouLoopk.size();i++) {
			List<List<ILoop>> s=unTouLoopk.get(i);
			for(int j=0;j<s.size();j++) {
				double v=1;
				for(int k=0;k<s.get(j).size();k++) {
					v=v*s.get(j).get(k).getLoopValue();
				}
				delta=delta+Math.pow(1,i)*v;
			}
		}
		return delta;
	}

	public List<ILoop> removeLoops(IPath p){
		List<ILoop> loopsk=new ArrayList<ILoop>();
		for(int i=0;i<loops.size();i++) {
			ILoop tILoop=new Loop(loops.get(i).getLoopValue(),loops.get(i).getTheNode(),loops.get(i).getLoopNodes(),loops.get(i).getLoopEdges());
			loopsk.add(tILoop);
		}
		for(int i=0;i<loopsk.size();i++) {
			for(int j=0;j<p.getPathNodes().size();j++) {
				if(loopsk.get(i).getLoopNodes().contains(p.getPathNodes().get(j))) {
					loopsk.get(i).setLoopValue(0);
					break;
				}
			}
		}
		return loopsk;
	}
	@Override
	public double getGain() {
		double num=0;
		for(int i=0;i<paths.size();i++) {
			double dk=calDeltaK(paths.get(i));
			num=num+dk*paths.get(i).getPathValue();
		}
		double gain=num/calDelta();
		return gain;
	}

}
