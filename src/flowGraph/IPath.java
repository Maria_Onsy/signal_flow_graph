package flowGraph;

import java.util.List;

public interface IPath {
	
/*
 * set the nodes of the path	
 */
public void setPathNodes(List<INode> nodes);
/*
 * return the nodes of the path	
 */
public List<INode> getPathNodes();
/*
 * set the edges of the path	
 */
public void setPathEdges(List<IEdge> edges);
/*
 * return the edges of the path	
 */
public List<IEdge> getPathEdges();
/*
 * set the total value of the path	
 */
public void setPathValue(double value);
/*
 * return the total value of the path	
 */
public double getPathValue();
}
