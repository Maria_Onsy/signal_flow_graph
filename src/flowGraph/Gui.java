package flowGraph;

import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Panel;
import java.awt.Point;

import javax.naming.spi.DirStateFactory.Result;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import java.awt.Font;

import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Dialog.ModalExclusionType;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JScrollBar;
import javax.xml.crypto.NodeSetData;

import java.awt.Color;

import javax.swing.JTextArea;
import javax.swing.DropMode;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;

import javax.swing.border.LineBorder;

import java.awt.Button;

import javax.swing.JDesktopPane;

public class Gui {

	private JFrame frmMasonFormula;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Gui window = new Gui();
					window.frmMasonFormula.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Gui() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
 
		final IGraph graph=new Graph();
		final DrawGraph draw=new Draw(graph);
				
		frmMasonFormula = new JFrame();
		frmMasonFormula.setResizable(false);
		frmMasonFormula.setTitle("Mason Formula");
		frmMasonFormula.setBounds(100, 100, 1166, 707);
		frmMasonFormula.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMasonFormula.getContentPane().setLayout(null);
		
		final JLabel gainRes = new JLabel("");
		gainRes.setFont(new Font("Tahoma", Font.PLAIN, 25));
		gainRes.setBounds(803, 576, 166, 48);
		frmMasonFormula.getContentPane().add(gainRes);
		
		final JLabel lblNewLabel = new JLabel("over all transfer function:");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblNewLabel.setBounds(456, 591, 297, 33);
		lblNewLabel.setVisible(false);
		frmMasonFormula.getContentPane().add(lblNewLabel);
		

		final JPanel panel = new JPanel() {@Override
			public void paintComponent(Graphics g) {
			super.paintComponent(g);
				draw.refresh(g);
			}
		};
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel.setBounds(272, 35, 860, 510);
		frmMasonFormula.getContentPane().add(panel);
		
		
		JButton btnNewButton = new JButton("Calucualte over all transfer function");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String n1=JOptionPane.showInputDialog(frmMasonFormula,"From Node:");
				if(n1==null) {
					JOptionPane.showMessageDialog(null, "No node is enterd", "Error", JOptionPane.ERROR_MESSAGE);
					return;}
				INode from=graph.getNode(n1);
				if(!graph.getNodes().contains(from)) {
					JOptionPane.showMessageDialog(null, "There isn't such a node", "Error", JOptionPane.ERROR_MESSAGE);
				    return;
				}
				String n2=JOptionPane.showInputDialog(frmMasonFormula,"To Node:");
				if(n2==null) {
					JOptionPane.showMessageDialog(null, "No node is enterd", "Error", JOptionPane.ERROR_MESSAGE);
					return;}
				INode to=graph.getNode(n2);
				if(!graph.getNodes().contains(to)) {
					JOptionPane.showMessageDialog(null, "There isn't such a node", "Error", JOptionPane.ERROR_MESSAGE);
				    return;
				}
				IFormula f=new MasonFormula(graph,from ,to);
				lblNewLabel.setVisible(true);
				gainRes.setText(Double.toString(f.getGain()));
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnNewButton.setBounds(15, 576, 371, 61);
		frmMasonFormula.getContentPane().add(btnNewButton);
		
		
		
				
		final JButton btnNewButton_1 = new JButton("Insert Node");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String name=JOptionPane.showInputDialog(frmMasonFormula,"Enter Node Name");
				if(name==null) {
					JOptionPane.showMessageDialog(null, "No name is enterd", "Error", JOptionPane.ERROR_MESSAGE);
					return;}
				INode n=new Node();
				n.setName(name);
				graph.insertNode(n);
				panel.repaint();
			}
		});
		btnNewButton_1.setBounds(15, 16, 150, 48);
		frmMasonFormula.getContentPane().add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Insert Edge");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String n1=JOptionPane.showInputDialog(frmMasonFormula,"From Node:");
				if(n1==null) {
					JOptionPane.showMessageDialog(null, "No node is enterd", "Error", JOptionPane.ERROR_MESSAGE);
					return;}
				INode from=graph.getNode(n1);
				if(!graph.getNodes().contains(from)) {
					JOptionPane.showMessageDialog(null, "There isn't such a node", "Error", JOptionPane.ERROR_MESSAGE);
				    return;
				}
				String n2=JOptionPane.showInputDialog(frmMasonFormula,"To Node:");
				if(n2==null) {
					JOptionPane.showMessageDialog(null, "No node is enterd", "Error", JOptionPane.ERROR_MESSAGE);
					return;}
				INode to=graph.getNode(n2);
				if(!graph.getNodes().contains(to)) {
					JOptionPane.showMessageDialog(null, "There isn't such a node", "Error", JOptionPane.ERROR_MESSAGE);
				    return;
				}
				String v=JOptionPane.showInputDialog(frmMasonFormula,"Enter Value");
				if(v==null) {
					JOptionPane.showMessageDialog(null, "No Value is enterd", "Error", JOptionPane.ERROR_MESSAGE);
					return;}
				double value=0;
				try {
					value=Double.valueOf(v);
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, "Enter correct Value", "Error", JOptionPane.ERROR_MESSAGE);
	                return;			
				}
				if(value==0) {
					JOptionPane.showMessageDialog(null, "Edge value can't equal to 0 ", "Error", JOptionPane.ERROR_MESSAGE);
					return;}
				IEdge edge=new Edge(value,value>0, to,from);
				graph.insertEdge(edge);
				btnNewButton_1.setVisible(false);
				panel.repaint();
			}
		});
		btnNewButton_2.setBounds(15, 80, 150, 48);
		frmMasonFormula.getContentPane().add(btnNewButton_2);
		
		JButton btnClearGraph = new JButton("Clear Graph");
		btnClearGraph.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				graph.clear();
				draw.clear();
				panel.repaint();
				lblNewLabel.setVisible(false);
				btnNewButton_1.setVisible(false);
			}
		});
		btnClearGraph.setBounds(15, 144, 150, 48);
		frmMasonFormula.getContentPane().add(btnClearGraph);
		
		JButton btnShowPaths = new JButton("show paths");
		btnShowPaths.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(graph.getNodes().size()==0) {
					JOptionPane.showMessageDialog(null, "No nodes is enterd", "Error", JOptionPane.ERROR_MESSAGE);
				    return;
				}
				List<IPath> paths=graph.getPaths(graph.getNodes().get(0), graph.getNodes().get(graph.getNodes().size()-1));
				String p="Paths:";
				for(int i=0;i<paths.size();i++) {
					p=p+"\n"+(i+1)+". Nodes: ";
					List<INode> n=paths.get(i).getPathNodes();
					for(int j=0;j<n.size();j++) {
						if(j==n.size()-1) {p=p+n.get(j).getName();}
						else{p=p+n.get(j).getName()+", ";}
					}
					p=p+"\n  value:"+Double.valueOf(paths.get(i).getPathValue());
				}
				JOptionPane.showMessageDialog(null,p );
			}
		});
		btnShowPaths.setBounds(15, 233, 150, 48);
		frmMasonFormula.getContentPane().add(btnShowPaths);
		
		JButton btnShowLoops = new JButton("show loops");
		btnShowLoops.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(graph.getNodes().size()==0) {
					JOptionPane.showMessageDialog(null, "No nodes is enterd", "Error", JOptionPane.ERROR_MESSAGE);
				    return;
				}
				List<ILoop> loops=graph.getLoops();
				String p="Loops:";
				for(int i=0;i<loops.size();i++) {
					p=p+"\n"+(i+1)+". Nodes: ";
					List<INode> n=loops.get(i).getLoopNodes();
					for(int j=0;j<n.size();j++) {
						if(j==n.size()-1) {p=p+n.get(j).getName();}
						else{p=p+n.get(j).getName()+", ";}
					}
					p=p+"\n  value:"+Double.valueOf(loops.get(i).getLoopValue());
				}
				JOptionPane.showMessageDialog(null,p );
			}
		});
		btnShowLoops.setBounds(15, 297, 150, 48);
		frmMasonFormula.getContentPane().add(btnShowLoops);
		
		JButton btnShowUntouchedLoops = new JButton("show untouched loops");
		btnShowUntouchedLoops.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(graph.getNodes().size()==0) {
					JOptionPane.showMessageDialog(null, "No nodes is enterd", "Error", JOptionPane.ERROR_MESSAGE);
				    return;
				}
				String ns=JOptionPane.showInputDialog(null, "Enter n");
				int nr=Integer.valueOf(ns)-2;
				List<List<List<ILoop>>> un=graph.getUnTouLoops(graph.getLoops());
				if(un.size()==0||un.size()-1<nr) {
					JOptionPane.showMessageDialog(null, "No untouched loops for this n", "Error", JOptionPane.ERROR_MESSAGE);
				    return;
				}
				List<List<ILoop>> l=un.get(nr);
				if(l.size()==0) {
					JOptionPane.showMessageDialog(null, "No untouched loops for this n", "Error", JOptionPane.ERROR_MESSAGE);
				    return;
				}
				String p="Untouched Loops:";
				for(int k=0;k<l.size();k++) {
				List<ILoop> loops=l.get(k);
				p=p+"\n"+(k+1)+".";
				for(int i=0;i<loops.size();i++) {
					p=p+"\n"+(i+1)+". Nodes: ";
					List<INode> n=loops.get(i).getLoopNodes();
					for(int j=0;j<n.size();j++) {
						if(j==n.size()-1) {p=p+n.get(j).getName();}
						else{p=p+n.get(j).getName()+", ";}
					}
					p=p+"\n  value:"+Double.valueOf(loops.get(i).getLoopValue());
				}}
				JOptionPane.showMessageDialog(null,p );
			}
		});
		btnShowUntouchedLoops.setBounds(15, 361, 218, 48);
		frmMasonFormula.getContentPane().add(btnShowUntouchedLoops);
		
		JButton btnCalculateDelta = new JButton("calculate delta");
		btnCalculateDelta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				IFormula f=new MasonFormula(graph,graph.getNodes().get(0) ,graph.getNodes().get(graph.getNodes().size()-1));
                String p="Total delta="+f.calDelta();	
                List<IPath> paths=graph.getPaths(graph.getNodes().get(0), graph.getNodes().get(graph.getNodes().size()-1));
				for(int i=0;i<paths.size();i++) {
					p=p+"\nDelta"+(i+1)+" = "+f.calDeltaK(paths.get(i));
				}
				JOptionPane.showMessageDialog(null,p );
			}
		});
		btnCalculateDelta.setBounds(15, 425, 150, 48);
		frmMasonFormula.getContentPane().add(btnCalculateDelta);
		
		
	}
}
