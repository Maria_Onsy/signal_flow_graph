package flowGraph;

import java.util.List;

public interface INode {
/*
 *set the name of the node
 */
public void setName(String name);
/*
 * return the name of the node
 */
public String getName();
/*
 * set the neighbor nodes which connected with positive edges 
 */
public void setNextNodes(List<INode> nodes);
/*
 * return the neighbor nodes which connected with positive edges 
 */
public List<INode> getNextNodes();
/*
 * set the neighbor nodes which connected with negative edges 
 */
public void setPreviousNodes(List<INode> nodes);
/*
 * return the neighbor nodes which connected with negative edges 
 */
public List<INode> getPreviousNodes();
/*
 * set the node positive edges
 */
public void setForwardEdges(List<IEdge> edges);
/*
 * return the node positive edges
 */
public List<IEdge> getForwardEdges();
/*
 * set the node negative edges
 */
public void setBackwardEdges(List<IEdge> edges);
/*
 * return the node negative edges
 */
public List<IEdge> getBackwardEdges();
}
