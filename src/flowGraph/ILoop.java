package flowGraph;

import java.util.List;

public interface ILoop {
	
   /*
    * set the repeated node	
    */
	public void setTheNode(INode node);
	/*
	 * return the repeated node	
	 */
	public INode getTheNode();
	/*
	 * set the nodes of the loop	
	 */
	public void setLoopNodes(List<INode> nodes);
	/*
	 * return the nodes of the loop
	 */
	public List<INode> getLoopNodes();
	/*
	 * set the edges of the loop	
	 */
	public void setLoopEdges(List<IEdge> edges);
	/*
	 * return the edges of the loop	
	 */
	public List<IEdge> getLoopEdges();
	/*
	 * set the total value of the loop	
	 */
	public void setLoopValue(double value);
	/*
	 * return the total value of the loop	
	 */
	public double getLoopValue();
}
